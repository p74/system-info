package ir.p74.projects.util.sysinfo;

public class UnitConvetion {
    /**
     * Converts bytes to higher values (kB, MB, GB, ...)
     * to be more readable by human.
     *
     * Thanks to this dude:
     * https://stackoverflow.com/users/276052/aioobe
     *
     * Example output:
     *                               SI     BINARY
     *
     *                    0:        0 B        0 B
     *                   27:       27 B       27 B
     *                  999:      999 B      999 B
     *                 1000:     1.0 kB     1000 B
     *                 1023:     1.0 kB     1023 B
     *                 1024:     1.0 kB    1.0 KiB
     *                 1728:     1.7 kB    1.7 KiB
     *               110592:   110.6 kB  108.0 KiB
     *              7077888:     7.1 MB    6.8 MiB
     *            452984832:   453.0 MB  432.0 MiB
     *          28991029248:    29.0 GB   27.0 GiB
     *        1855425871872:     1.9 TB    1.7 TiB
     *  9223372036854775807:     9.2 EB    8.0 EiB   (Long.MAX_VALUE)
     *
     * @param bytes input value in bytes
     * @param si if set <code>true</code>, the resault will be in SI. Otherwise
     *           it will be in Binary. (Ex: 1024B = 1.0 kB (SI) = 1.0 KiB (Bin))
     * @return the result in a human-readable format
     */
    public static String humanReadableByteCount(long bytes, boolean si) {
        int unit = si ? 1000 : 1024;
        if (bytes < unit) return bytes + " B";
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp-1) + (si ? "" : "i");
        return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
    }

    public static String humanReadableByteCount(long bytes) {
        return humanReadableByteCount(bytes, true);
    }
}
