package ir.p74.projects.util.sysinfo;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

public class SysInfo {
    public static void FileSysMemory() {
        File[] roots = File.listRoots();

        String jsonRoots = "";
        for (File root : roots) {
            jsonRoots += new JSONObject()
                    .put("AbsPath", root.getAbsolutePath())
                    .put("TotalSpace",root.getTotalSpace())
                    .put("FreeSpace",root.getFreeSpace())
                    .put("UsableSpace",root.getUsableSpace())
                    .toString();
        }
        //jsonRoots += "}";

        System.out.println(jsonRoots);

    }



}
